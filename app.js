let num = 42;
let firstName = "Oybek";
const isProgrammer = true;
/* 
// Can do
let $ = "test";
let $num = 42;
let num$ = 42;
let _ = 49;
let _num = 12;
let num_ = 12;
let firt_name = "Elena"; // bad
let myNum = 34; // good
let num42 = 10;
*/

/*  
Restricted
// let 42num = '11'
// let my-num = 1
// let false 
*/

// console.log(num);
const resultElement = document.getElementById("result");
const input1 = document.getElementById("input1");
const input2 = document.getElementById("input2");
const btnPlus = document.getElementById("plus");
const btnMinus = document.getElementById("minus");
const btnSubmit = document.getElementById("submit");
let action = "";
btnPlus.onclick = function () {
  action = "+";
};
btnMinus.onclick = function () {
  action = "-";
};
function resultColour(summ) {
  if (summ >= 0) {
    resultElement.style.color = "green";
  } else if (summ < 0) {
    resultElement.style.color = "red";
  }
  resultElement.textContent = summ;
}
function resultCalc(input1, input2, action) {
  const summ1 = Number(input1.value);
  const summ2 = Number(input2.value);
  if (action == "+") {
    return summ1 + summ2;
  } else if (action == "-") {
    return summ1 - summ2;
  } else {
    return "Iltimos kerakli amalni tanlang";
  }
}
btnSubmit.addEventListener("click", function () {
  const result = resultCalc(input1, input2, action);
  resultColour(result);
});
